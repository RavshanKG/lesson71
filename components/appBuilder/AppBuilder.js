import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import axios from 'axios';

export default class AppBuilder extends React.Component {
  state = {
    result: []
  }

  componentDidMount() {

    axios.get('https://www.reddit.com/r/pics.json').then(response => {
      let res = response.data.data.children.map((item, id) => {
        return {thumbnail: item.data.thumbnail, title: item.data.title, id: item.data.id}

      });
      this.setState({result: res})
    })
  }


  render() {
    console.log('state', this.state.result)
    return (
      <View style={styles.container}>
        {this.state.result.map(post => (
          <View style={styles.picText}>
            <Text>{post.title}</Text>
            <Image source={{uri: post.thumbnail}} style={{width: 50, height: 50, marginRight: 10
              }}/>
          </View>
        ))}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  picText: {
    width: '100%',
    backgroundColor: '#eee',
    marginBottom: 10,
    padding: 10

  }
});
